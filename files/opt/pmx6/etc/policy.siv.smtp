require "PureMessage";

# attr NAME=Log email data
if true
{
    pmx_log "5" "LOG ;; relay=%%SENDER_IP%% ;; from=%%ENVELOPE_FROM%% ;; to=%%ENVELOPE_TO%% ;; size=%%MESSAGE_SIZE%% ;; subject=%%SUBJECT%%";
}
# attr NAME=Discard email with scannable viruses
if pmx_virus
{
    # attr NAME=Discard scannable viruses
    if not pmx_virus_cantscan
    {
        pmx_log "5" "LOG ;; discard ;; virus=%%VIRUS_IDS%%";
        discard;
        stop;
    }
}
# attr NAME=Reject mail from blacklist senders and hosts
if anyof(address :all :memberof :comparator "i;ascii-casemap"
                 ["from"] ["blacklisted-senders"],
         pmx_relay :memberof ["blacklisted-hosts"])
{
    pmx_log "5" "LOG ;; reject ;; blacklist=blacklist-senders";
    reject "Stanford email policy violation: blacklisted senders";
    stop;
}
# attr NAME=Tag suspicious attachments
if allof(pmx_suspect_attachment,
         not header :comparator "i;ascii-casemap" :contains
                    ["Subject"] ["[POSSIBLE VIRUS:###]"],
         not address :all :memberof :comparator "i;ascii-casemap"
                    ["from"] ["trusted-senders"])
{
    pmx_log "5" "LOG ;; continue ;; tag ;; suspicious";
    pmx_replace_header :all "Subject" "[POSSIBLE VIRUS:###] %%SUBJECT%%";
}
# attr NAME=Secure email policy
if anyof(header :comparator "i;ascii-casemap" :memberof
                ["Subject"] ["secure-words"],
         header :comparator "i;ascii-casemap" :contains
                ["Received"] ["res.cisco.com"])
{
    # attr NAME=Secure email list policy
    if envelope :comparator "i;ascii-casemap" :all :memberof
                      ["to"] ["secure-blacklist-recipients"]
    {
        # attr NAME=Accept secure email to secure lists
        if envelope :comparator "i;ascii-casemap" :all :re
                    ["to"] ["^secure-"]
        {
            pmx_log "5" "LOG ;; accept ;; secure ;; securelist";
            keep;
            stop; 
        }
        # attr NAME=Reject secure email to insecure lists
        else
        {
            pmx_log "5" "LOG ;; reject ;; secure ;; blacklist";
            reject "Stanford email policy violation: insecure email list";
            stop;
        }
    }
    # attr NAME=Append footer to mail to trusted recipients
    if envelope :comparator "i;ascii-casemap" :all :memberof
                ["to"] ["secure-trusted-recipients"]
    {
        pmx_log "5" "LOG ;; continue ;; secure ;; tag";
        pmx_log "5" "trusty: %%ENVELOPE_FROM%% => %%ENVELOPE_TO%%";
        pmx_add_banner :body :data "This message was encrypted in transit via the Stanford Secure Email service.";
    }
    # attr NAME=Tag mail to untrusted recipients
    else
    {
        pmx_log "5" "LOG ;; accept ;; secure ;; encrypt";
        pmx_log "5" "secure: %%ENVELOPE_FROM%% => %%ENVELOPE_TO%%";
        pmx_replace_header :all "X-Encrypt" "CRES";
        keep;
        stop;
    }
}
# attr NAME=Reject or discard high probability spam 
if not pmx_spam_prob :under 90
{
    # attr NAME=Deliver mail from whitelist senders
    if anyof(pmx_relay :memberof ["whitelisted-hosts"],
             address :comparator "i;ascii-casemap" :all :memberof
                     ["from"] ["whitelisted-senders"],
             envelope :comparator "i;ascii-casemap" :all :memberof
                      ["from"] ["whitelisted-senders"],
             envelope :comparator "i;ascii-casemap" :all :memberof
                      ["to"] ["anti-spam-optouts"])
    {
        pmx_log "5" "LOG ;; accept ;; whitelist ;; spam=%%PROB%%";
        pmx_add_header "X-Grey" "yes";
        keep;
        stop;
    }
    # attr NAME=Reject mail from envelope Stanford senders
    if envelope :comparator "i;ascii-casemap" :all :re ["from"]
                ["[^@]+@(.+\\.)*stanford\\.edu"]
    {
        pmx_log "5" "LOG ;; reject ;; spam=%%PROB%%";
        reject "Stanford email policy violation: high probability spam";
        stop;
    }
    # attr NAME=Discard
    if true
    {
        pmx_log "5" "LOG ;; discard ;; spam=%%PROB%%";
        discard;
        stop;
    }
}
# attr NAME=Tag and deliver probable spam if not whitelisted
if not pmx_spam_prob :under 50
{
    pmx_add_header "X-Grey" "yes";
    # attr NAME=Deliver mail from whitelisted hosts and senders
    if anyof(pmx_relay :memberof ["whitelisted-hosts"],
             address :comparator "i;ascii-casemap" :all :memberof
                     ["from"] ["whitelisted-senders"],
             envelope :comparator "i;ascii-casemap" :all :memberof
                      ["from"] ["whitelisted-senders"],
             envelope :comparator "i;ascii-casemap" :all :memberof
                      ["to"] ["anti-spam-optouts"])
    {
        pmx_log "5" "LOG ;; accept ;; whitelist ;; spam=%%PROB%%";
        keep;
        stop;
    }
    # attr NAME=Tag and deliver
    if true
    {
        pmx_log "5" "LOG ;; accept ;; tag ;; spam=%%PROB%%";
        pmx_replace_header :all "Subject" "[SPAM:%%GAUGE%%] %%SUBJECT%%";
        pmx_replace_header :all
            "X-Spam" "Probability=%%PROB%%, Report='%%HITS%%'";
        keep;
        stop;
    }
}
# attr NAME=Log final action
if true
{
    pmx_log "5" "LOG ;; accept ;; spam=%%PROB%%";
}
