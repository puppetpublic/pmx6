##############################################################################
# The smtp class models
##############################################################################

class pmx6::smtp::test inherits pmx6::smtp {
  Pmx6::File ['policy.siv'] {
    source => 'puppet:///modules/pmx6/opt/pmx6/etc/policy.siv.smtp.test',
  }
}
