##############################################################################
# The smtp class models
##############################################################################

class pmx6::smtp::grey inherits pmx6::smtp {
  # PureMessage postgres performance tweaks.
  base::sysctl {
    'kernel.shmall': ensure => '268435456';
    'kernel.shmmax': ensure => '268435456';
  }

  # Install the reporting cron jobs.
  file { '/etc/cron.d/pmx-master':
    source => 'puppet:///modules/pmx6/etc/cron.d/pmx-master';
  }
}
