# Install pmx configuration directories.
define pmx6::dir(
  $ensure
) {
  case $ensure {
    present: {
      file { "/opt/pmx6/etc/${name}":
        ensure  => directory,
        owner   => 'pmx6',
        group   => 'pmx6',
        require => Package['stanford-pmx'];
      }
    }
    absent: {
      file { "/opt/pmx6/etc/${name}": ensure => absent }
    }
    default: { crit "Invalid ensure value: ${ensure}." }
  }
}
