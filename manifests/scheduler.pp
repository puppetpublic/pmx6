# Configure pmx internal scheduler job and restart pmx scheduler
define pmx6::scheduler($ensure) {
  $schedulerconf = "/opt/pmx6/etc/scheduler.d/${name}.conf"
  case $ensure {
    enabled: {
      base::filter { $schedulerconf:
        regex   => 'enabled = 0',
        value   => 'enabled = 1',
        require => Package['stanford-pmx'],
      }
    }
    disabled: {
      base::filter { $schedulerconf:
        regex   => 'enabled = 1',
        value   => 'enabled = 0',
        require => Package['stanford-pmx'],
      }
    }
    default: { crit "Invalid ensure value (enabled,disabled): ${ensure}." }
  }
}
