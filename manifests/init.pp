# Standard pmx classes for s_emailrouters.
#   class pmx6:         pmx generic edge server model
#   class pmx6::mx:     pmx mx edge server model
#   class pmx6::smtp:   pmx smtp edge server model
#   class pmx6::master: pmx database server model

# Standard PureMessage edge module.
class pmx6 {
  if ($::operatingsystem == 'Debian' and $::lsbmajdistrelease >= 7) { # wheezy+
    $pmx_architecture=':i386'
    exec {'dpkg --add-architecture i386':
      command => 'dpkg --add-architecture i386',
      unless  => 'dpkg --print-foreign-architectures | grep -q i386',
      notify  => Package['stanford-pmx'],
    }
  }
  package { "stanford-pmx${pmx_architecture}":
    ensure  => installed,
    alias   => 'stanford-pmx'
  }

  # Install the reporting cron jobs.
  file {
    '/etc/cron.d/pmx':
      source => 'puppet:///modules/pmx6/etc/cron.d/pmx';
    '/etc/remctl/conf.d/monitor-pmx':
      source => 'puppet:///modules/pmx6/etc/remctl/conf.d/monitor-pmx';
    '/etc/remctl/conf.d/pmx':
      source => 'puppet:///modules/pmx6/etc/remctl/conf.d/pmx';
    '/etc/filter-syslog/pmx':
      source => 'puppet:///modules/pmx6/etc/filter-syslog/pmx';
    '/etc/newsyslog.daily/pmx':
      source => 'puppet:///modules/pmx6/etc/newsyslog.daily/pmx';
    '/var/log/pmx':
      ensure => directory;
    '/var/log/pmx/blocklist_log':
      ensure => present;
    '/var/log/pmx/message_log':
      ensure => present;
  }

  # Iptable rules for pmx servers.
  base::iptables::fragment { 'pmx': ensure => present }

  # # Local pmx configuration files and lists.
  pmx6::dir {
    'lists.d': ensure => present;
    'pmx.d':   ensure => present;
  }
  pmx6::file {
    'pmx.conf':
      ensure  => present,
      restart => true;
    'policy.siv':
      ensure  => present,
      source  => 'puppet:///modules/pmx6/opt/pmx6/etc/policy.siv.generic',
      restart => true;
    'anti-spam-optouts':                        ensure => present;
    'blacklisted-hosts':                        ensure => present;
    'blacklisted-senders':                      ensure => present;
    'ip-blocking-exceptions':                   ensure => present;
    'whitelisted-hosts':                        ensure => present;
    'whitelisted-senders':                      ensure => present;
    'internal-senders':                         ensure => present;
    'lists.d/internal-senders.conf':            ensure => present;
    'trusted-senders':                          ensure => present;
    'lists.d/trusted-senders.conf':             ensure => present;
    'secure-blacklist-recipients':              ensure => present;
    'lists.d/secure-blacklist-recipients.conf': ensure => present;
    'secure-trusted-recipients':                ensure => present;
    'lists.d/secure-trusted-recipients.conf':   ensure => present;
    'secure-words':                             ensure => present;
    'lists.d/secure-words.conf':                ensure => present;
    'pmx.d/quarantine_expire.conf':             ensure => present;
  }

  # Enable pmx blocklist updates.
  pmx6::scheduler {
    'consume-blocklist-log':     ensure => enabled;
    'pmx-blocklist-data-update': ensure => enabled;
  }

  # Add zip files to the list of suspicious attachments.
  base::textline { '*.zip':
    ensure  => '/opt/pmx6/etc/suspect-attachment-names',
    require => Package['stanford-pmx'];
  }
}
