##############################################################################
# The smtp class models
##############################################################################

class pmx6::mx::test inherits pmx6::mx {
  Pmx6::File ['policy.siv'] {
    source => 'puppet:///modules/pmx6/opt/pmx6/etc/policy.siv.mx.test',
  }
}
