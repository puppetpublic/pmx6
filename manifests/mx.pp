##############################################################################
# The smtp class models
##############################################################################

#   class pmx6::mx:     pmx mx edge server model
class pmx6::mx inherits pmx6 {
  Pmx6::File ['policy.siv'] {
    source => 'puppet:///modules/pmx6/opt/pmx6/etc/policy.siv.mx',
  }
}
