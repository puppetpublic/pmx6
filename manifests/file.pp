# Install pmx configuration files.
define pmx6::file(
  $ensure,
  $source = 'NOSRC',
  $restart = false
) {
  case $ensure {
    present: {
      file { "/opt/pmx6/etc/${name}":
        source  => $source ? {
          'NOSRC' => "puppet:///modules/pmx6/opt/pmx6/etc/${name}",
          default => $source,
        },
        owner   => 'pmx6',
        group   => 'pmx6',
        require => Package['stanford-pmx'];
      }
    }
    default: { crit "Invalid ensure value (present): ${ensure}." }
  }

  if $restart {
    exec { "restart ${name}":
      command     => '/usr/sbin/invoke-rc.d stanford-pmx restart',
      subscribe   => File["/opt/pmx6/etc/${name}"],
      refreshonly => true,
    }
  }
}
