##############################################################################
# The smtp class models
##############################################################################

#   class pmx6::smtp:   pmx smtp edge server model
class pmx6::smtp inherits pmx6 {
  Pmx6::File ['policy.siv'] {
    source => 'puppet:///modules/pmx6/opt/pmx6/etc/policy.siv.smtp',
  }

  base::filter { '/opt/pmx6/etc/spam.d/sxl.conf':
    regex   => 'enable      = __DEFAULT__',
    value   => 'enable      = 0',
    require => Package['stanford-pmx'];
  }

  #   base::filter { '/opt/pmx6/etc/spam.conf':
  #       regex   => 'local_tests_only = __DEFAULT__',
  #       value   => 'local_tests_only = 1',
  #       require => Package['stanford-pmx'];
  #   }
}
